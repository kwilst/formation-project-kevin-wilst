FROM node:12-alpine
COPY . /app
WORKDIR /app
EXPOSE 8000
CMD ["npm","start"]
